let coursesData;
export default coursesData = [{
    id:'wdc001',
    name:'PHP-Laravel',
    description:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam veniam voluptates, aspernatur laudantium accusantium recusandae explicabo culpa autem. Dicta minima culpa magnam eveniet vero alias laborum dolores aliquam beatae labore.',
    price: 25000,
    onOffer: true
},{
    id:'wdc002',
    name:'Python-Django',
    description:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam veniam voluptates, aspernatur laudantium accusantium recusandae explicabo culpa autem. Dicta minima culpa magnam eveniet vero alias laborum dolores aliquam beatae labore.',
    price: 35000,
    onOffer: true
},{
    id:'wdc003',
    name:'Java-Springboot',
    description:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam veniam voluptates, aspernatur laudantium accusantium recusandae explicabo culpa autem. Dicta minima culpa magnam eveniet vero alias laborum dolores aliquam beatae labore.',
    price: 25000,
    onOffer: true
},{
    id:'wdc004',
    name:'NodeJS-ExpressJS',
    description:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam veniam voluptates, aspernatur laudantium accusantium recusandae explicabo culpa autem. Dicta minima culpa magnam eveniet vero alias laborum dolores aliquam beatae labore.',
    price: 55000,
    onOffer: false
}]