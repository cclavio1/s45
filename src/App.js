import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AppNavBar from './components/AppNavbar';
import Footer from './components/Footer';
import Courses from './pages/Courses';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound'
import {UserProvider} from './UserContext';
import { useState } from 'react';

function App() {

  
  const [user, setUser]= useState({
    id:1,
    isAdmin:null
  })

  return (
  <UserProvider value={{user,setUser}}>
    <BrowserRouter>
     <AppNavBar />
        <Routes>
          <Route path="/" element={<Home/> }/>
          <Route path="/courses" element={<Courses/> }/>
          <Route path="/register" element={<Register/> }/>
          <Route path="/login" element={<Login/> }/>
          <Route path="/logout" element={<Logout/> }/>
          <Route path="*" element={<NotFound/>}/>
        </Routes>
      <Footer />
     </BrowserRouter>
     </UserProvider>
  )
}

export default App;
