import { useContext } from "react";
import CourseCard from "../components/CourseCard"
import coursesData from "../mockData/courses"
import UserContext from "../UserContext";

export default function Courses(){
    const {user,setUser}= useContext(UserContext);
    console.log(user)
const courses = coursesData.map(course=>{
    return<CourseCard key={course.id} courseProp={course}/>
})
    return(
        courses
    )
}