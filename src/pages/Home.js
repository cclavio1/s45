
import Banner from '../components/Banner';
import { Fragment } from 'react';
import Highlights from '../components/Highlights';


export default function Home(){
    let bannerMessage = {
        head:"Welcome to Course Booking App!",
        body:"Opportunities for everyone, everywhere",
        button:"Click here",
        destination:"/"
    }
    return(
        <Fragment>
            <Banner message={bannerMessage} />
            <Highlights />
        </Fragment>
    )
}