import{Form,Button,Row,Col,Container} from 'react-bootstrap'
import {useState,useEffect} from 'react'
import {useNavigate} from 'react-router-dom'


export default function Login(){

    let [email,setEmail] = useState('')
    let [pw,setPW] = useState('')
    let [isDisabled,setIsDisabled] = useState(true)

    let navigate = useNavigate()
    
    let login=(e)=>{
        e.preventDefault()
        fetch('https://polar-citadel-98467.herokuapp.com/api/users/login', {
			method: "POST",
			headers: {
			"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: pw,
			})
		})
        .then(result=>result.json())
        .then(result=>{
            if(result.status=="error"){
                alert('wrong email or password')
            }else{
                 localStorage.setItem('token',result.token)
                 localStorage.setItem('email',email)

                navigate("/courses")
            }
        })
        
    }

    useEffect((e)=>{
        if(email==""||pw==""){
            setIsDisabled(true)
        }else{
            setIsDisabled(false)
        }
    })

    return(
        <Container className="m-5">
                <h3 className='text-center'>Login</h3>
            <Row className="justify-content-center">
                <Col xs={12} md={6}>
                <Form onSubmit={(e)=>login(e)}>

                <Form.Group className="mb-3">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" value={email} onChange={(e)=>{setEmail(e.target.value)}}/>
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" value={pw} onChange={(e)=>{setPW(e.target.value)}}/>
                </Form.Group>

                
                <Button variant="info" type="submit" disabled={isDisabled}>
                    Submit
                </Button>
            </Form>
                </Col>
            </Row>
        </Container>
    )
}