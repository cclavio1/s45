import Banner from "../components/Banner";


export default function NotFound(){

    let message= {
        head:"Zuitt Booking",
        body:"Page not Found",
        button:"Go back",
        destination:"/"
    }
    return(
        <Banner message={message}/>
    )
}