import { Card, Row, Col } from "react-bootstrap"


export default function Highlights(){
    return(
            <Row>
                <Col xs={12} md={4}>
                <Card>
                    <Card.Body>     
                    <Card.Title>Learn From Home</Card.Title>
                    <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam veniam voluptates, aspernatur laudantium accusantium recusandae explicabo culpa autem. Dicta minima culpa magnam eveniet vero alias laborum dolores aliquam beatae labore.
                    </Card.Text>
                    </Card.Body>
                </Card>
                </Col>
                <Col xs={12} md={4}>
                <Card>
                    <Card.Body>     
                    <Card.Title>Study Now Pay Later</Card.Title>
                    <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam veniam voluptates, aspernatur laudantium accusantium recusandae explicabo culpa autem. Dicta minima culpa magnam eveniet vero alias laborum dolores aliquam beatae labore.
                    </Card.Text>
                    </Card.Body>
                </Card>
                </Col>
                <Col xs={12} md={4}>
                <Card>
                    <Card.Body>     
                    <Card.Title>Be Part of Our Community</Card.Title>
                    <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam veniam voluptates, aspernatur laudantium accusantium recusandae explicabo culpa autem. Dicta minima culpa magnam eveniet vero alias laborum dolores aliquam beatae labore.
                    </Card.Text>
                    </Card.Body>
                </Card>
                </Col>
            </Row>
    )
}