


export default function Banner({message}){
    return(
    <div className="jumbotron jumbotron-fluid">
        <div className="container">
          <h1>{message.head}</h1>
          <p>{message.body}</p>
          <a href={message.destination} className="btn btn-info">{message.button}</a>
        </div>
    </div>

    )
}