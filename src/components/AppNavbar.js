import { Fragment } from 'react'
import {Navbar,Container,Nav} from 'react-bootstrap'

export default function AppNavBar(){

	const email = localStorage.getItem('email')

    return (
      <Navbar bg="info" expand="lg" className="sticky-top">
		  <Container>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="me-auto">
		        <Nav.Link href="/">Home</Nav.Link>
		        <Nav.Link href="/courses">Courses</Nav.Link>
				{email == undefined? 
				<Fragment>
					<Nav.Link href="/register">Register</Nav.Link>
					<Nav.Link href="/login">Login</Nav.Link>
				</Fragment>:
					<Nav.Link href="/logout">Logout</Nav.Link>
				}
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>

    )
}